from django.test import TestCase
from django.test import TestCase
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings
from .views import *

# Create your tests here.
class UnitTest(TestCase):
    def test_landing_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_url_is_redirect(self):
        response = Client().get('/welcome')
        self.assertEqual(response.status_code,302)

    def test_landing_url_doesnt_exist(self):
        response = Client().get('/test')
        self.assertEqual(response.status_code, 404)

    def test_landing_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_login_func(self):
        found = resolve('/')
        self.assertEqual(found.func, login_page)

    def test_welcome_func(self):
        found = resolve('/welcome')
        self.assertEqual(found.func, welcome)
    
    def test_logout_func(self):
        found = resolve('/logout')
        self.assertEqual(found.func, logout_view)

    def test_welcome_page_after_login(self):
        user = User.objects.create_user('asparagus', '', 'tomato')
        self.client.login(username='asparagus', password='tomato')
        response = self.client.get('/welcome')
        self.assertEqual(response.status_code, 200)

    def test_welcome_url_doesnt_exist(self):
        user = User.objects.create_user('asparagus', '', 'tomato')
        self.client.login(username='asparagus', password='tomato')
        response = self.client.get('/asparagus')
        self.assertEqual(response.status_code, 404)

    def test_welcome_using_index_template(self):
        user = User.objects.create_user('asparagus', '', 'tomato')
        self.client.login(username='asparagus', password='tomato')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)

    def test_logout_redirect(self):
        user = User.objects.create_user('asparagus', '', 'tomato')
        self.client.login(username='asparagus', password='tomato')
        response = self.client.post('/logout')
        self.assertEqual(response.status_code, 302)