from django.urls import path
from . import views

app_name = 'LoginApp'

urlpatterns = [
    path('', views.login_page, name = 'home'),
    path('welcome', views.welcome, name='welcome'),
    path('logout', views.logout_view, name='logout'),
]
